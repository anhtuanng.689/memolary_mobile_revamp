class Validators {
  static final RegExp _phoneRegex = RegExp(r'(\+84|0)\d{9}$');

  static final RegExp _emailRegex = RegExp(
      r'^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\])|(([a-zA-Z\-\d]+\.)+[a-zA-Z]{2,}))$');

  String checkPhoneNumber(String phoneNumber) {
    if (phoneNumber.isEmpty) return "Please input phone";

    if (!_phoneRegex.hasMatch(phoneNumber)) return "Phone not invalid";

    return '';
  }

  String checkEmail(String email) {
    if (email.isEmpty) return "Please input email";

    if (!_emailRegex.hasMatch(email)) return "Email not valid";

    return '';
  }

  String checkPass(String password) {
    int length = 6;
    if (password.isEmpty) return "Please input password";

    if (password.length < length) return "Password not invalid";

    return "";
  }
}
