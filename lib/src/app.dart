import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:memolary_mobile_revamp/src/pages/splash/splash.view.dart';
import 'package:sizer/sizer.dart';

import 'config/config.dart';
import 'constants/constants.dart';
import 'di/di.dart';
import 'pages/auth/auth.dart';
import 'pages/auth/presentation/auth/auth.bloc.dart';
import 'pages/theme/presentation/theme/theme.cubit.dart';
import 'routes/routes.dart';
import 'widgets/widgets.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: MultiBlocProvider(
          providers: [
            BlocProvider<AuthBloc>.value(
              value: getIt<AuthBloc>()
                ..add(const AuthEvent.checkAuthenticated()),
            ),
            BlocProvider<ThemeCubit>.value(
              value: getIt<ThemeCubit>()..setTheme(getIt<ThemeCubit>().getTheme()),
            ),
          ],
          child: BlocBuilder<ThemeCubit, ThemeState>(
            builder: (context, state) {
              return MaterialApp.router(
                routerConfig: AppRouter.router,
                debugShowCheckedModeBanner: false,
                title: 'Memolary App',
                themeMode: context.read<ThemeCubit>().state.themeMode,
                theme: AppTheme.lightTheme,
                darkTheme: AppTheme.darkTheme,
              );
            },
          ),
        ),
      );
    });
  }
}
