import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import '../constants/constants.dart';
import '../di/di.dart';
import '../pages/auth/auth.dart';
import '../pages/auth/presentation/auth/auth.bloc.dart';
import '../pages/courses/courses.dart';
import '../pages/home/home.view.dart';
import '../pages/splash/splash.view.dart';
import '../widgets/widgets.dart';
import 'router_refresh.dart';

class AppRouter {
  static final _rootNavigatorKey = GlobalKey<NavigatorState>();

  static final GoRouter _router = GoRouter(
    refreshListenable: AuthStateNotifier(getIt<AuthBloc>()),
    initialLocation: RouteConstants.splashPage,
    debugLogDiagnostics: true,
    navigatorKey: _rootNavigatorKey,
    routes: [
      GoRoute(
        name: RouteConstants.splashName,
        path: RouteConstants.splashPage,
       
        builder: (context, state) => const SplashPage(),
        redirect: (context, state) async {
          return splashRedirect(context, state);
        },
        
      ),
      GoRoute(
        name: RouteConstants.homeName,
        path: RouteConstants.homePage,
        builder: (context, state) => const HomePage(),
        routes: [
          GoRoute(
            name: RouteConstants.coursesName,
            path: RouteConstants.coursesPage,
            builder: (context, state) => BlocProvider(
              create: (context) => getIt<CoursesCubit>(),
              child: const CoursesPage(),
            ),
          ),
        ],
      ),
      GoRoute(
        name: RouteConstants.signInName,
        path: RouteConstants.signInPage,
        builder: (context, state) => BlocProvider(
          create: (context) => getIt<SignInCubit>(),
          child: const SignInPage(),
        ),
      ),
    ],
    errorBuilder: (context, state) => const NotFoundPage(),
  );

  static GoRouter get router => _router;

  // static String? authRedirect(BuildContext context, GoRouterState state) {
  //   bool isSignedIn =
  //       context.read<AuthBloc>().state.status == AuthStatus.authenticated;
  //   bool onSplashPage = state.location == RouteConstants.splashPage;
  //   if (isSignedIn && onSplashPage) {
  //     return RouteConstants.homePage;
  //   }
  //   if (!isSignedIn) {
  //     return RouteConstants.signInPage;
  //   }
  //   // if (!isSignedIn && !onSplashPage) {
  //   //   return '/${RouteConstants.signInPage}';
  //   // }
  //   return null;
  // }

  static String? splashRedirect(BuildContext context, GoRouterState state) {
    final authStatus = context.read<AuthBloc>().state.status;
    if (authStatus == AuthStatus.loading) {
      return null;
    }

    final isAuth = authStatus == AuthStatus.authenticated;
    final isSplash = state.location == RouteConstants.splashPage;

    if (isSplash) {
      return isAuth ? RouteConstants.homePage : RouteConstants.signInPage;
    }

    final isLoggingIn = state.location == RouteConstants.signInPage;
    if (isLoggingIn) {
      return isAuth ? RouteConstants.homePage : null;
    }

    return isAuth ? null : RouteConstants.splashPage;
  }
}
