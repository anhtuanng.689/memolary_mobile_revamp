import 'dart:async';

import 'package:flutter/material.dart';

import '../pages/auth/presentation/auth/auth.bloc.dart';

class AuthStateNotifier extends ChangeNotifier {
  late final StreamSubscription<AuthState> _authStream;
  AuthStateNotifier(AuthBloc bloc) {
    _authStream = bloc.stream.listen((event) {
      notifyListeners();
    });
  }

  @override
  void dispose() {
    _authStream.cancel();
    super.dispose();
  }
}

