import 'package:hive/hive.dart';

import '../constants/constants.dart';
import 'path_helper.dart';

class HiveStorage {
  static Future<void> initialBox() async {
    String path = await PathHelper.localStoreDirMemolary;
    Hive.init(path);
    await openBoxApp();
  }

  static Future<void> openBoxApp() async {
    await Hive.openBox(StorageKeys.boxAuth);
  }
}
