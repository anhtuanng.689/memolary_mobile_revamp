import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'colors.dart';

class AppTheme {
  AppTheme._();

  static ThemeData lightTheme = ThemeData.light(useMaterial3: true).copyWith(
    textTheme: GoogleFonts.beVietnamProTextTheme(ThemeData.light().textTheme),
    colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor,)
    ,
    primaryColorLight: AppColors.accentColor,
  );

  static ThemeData darkTheme = ThemeData.dark(useMaterial3: true).copyWith(
    textTheme: GoogleFonts.beVietnamProTextTheme(ThemeData.dark().textTheme),
    colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentDarkColor),
    primaryColorLight: AppColors.accentDarkColor,
  );
}
