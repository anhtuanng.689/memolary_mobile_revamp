import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const accentColor = Color(0xFF0F77F0);
  static const accentDarkColor = Color.fromARGB(255, 210, 231, 255);
  static const redColor = Color(0xFFE81C1C);
  static const textColor = Color(0xFF131318);
  static const buttonColor = Color.fromARGB(181, 50, 139, 241);
  static const buttonShadowColor = Color.fromARGB(255, 1, 18, 65);
  static const greenColor = Colors.green;
  static const whiteColor = Colors.white;
  static const transparentColor = Colors.transparent;
  static const blackColor = Colors.black;
  static const textFormColor = Color(0xFF898989);
  static const borderCardColor = Color(0xFFEAEAEA);
  static const searchBarFillColor = Color(0xFFF2F2F2);
  static const hintTextColor = Color(0xFF838383);
  static const blueTextColor = Color(0xFF0F77F0);
  static const dialogBackgroundColor = Color(0xFFF3F3F3);
}
