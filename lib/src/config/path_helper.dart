import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

import '../core/core.dart';

class PathHelper {
  static Future<void> deleteCacheImageDir(String path) async {
    final cacheDir = Directory(path);
    if (cacheDir.existsSync()) {
      cacheDir.deleteSync(recursive: true);
    }
  }

  static Future<void> createDirMemolary() async {
    String tempAskanyDir = await tempDirMemolary;
    String localStoreAskanyDir = await localStoreDirMemolary;
    Directory myDir = Directory(tempAskanyDir);
    Directory localDir = Directory(localStoreAskanyDir);
    if (!myDir.existsSync()) {
      await myDir.create();
    }

    if (!localDir.existsSync()) {
      await localDir.create();
    }
  }

  static Future<String> get tempDirMemolary async =>
      '${(await getTemporaryDirectory()).path}/memolary';

  static Future<String> get localStoreDirMemolary async =>
      '${(await getTemporaryDirectory()).path}/hive';

  static Future<Directory> get appDir async =>
      await getApplicationDocumentsDirectory();

  static Future<Directory?> get downloadsDir async {
    Directory downloadsDirectory;
    try {
      if (Platform.isIOS) {
        downloadsDirectory = await getLibraryDirectory();
      } else {
        downloadsDirectory = await getApplicationSupportDirectory();
      }

      return downloadsDirectory;
    } on PlatformException {
      logi.e('Could not get the downloads directory');
      return null;
    }
  }

  static Future<int> getTempSize() async {
    String tempAskanyDir = await tempDirMemolary;
    Directory myDir = Directory(tempAskanyDir);

    if (!myDir.existsSync()) return 0;

    return myDir.listSync().isEmpty ? 0 : ((myDir.statSync().size - 64) * 1024);
  }

  static Future<void> clearTempDir() async {
    String tempAskanyDir = await tempDirMemolary;
    Directory myDir = Directory(tempAskanyDir);

    if (!myDir.existsSync()) return;

    myDir.deleteSync(recursive: true);
    myDir.create();
  }
}
