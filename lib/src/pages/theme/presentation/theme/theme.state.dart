part of 'theme.cubit.dart';

@freezed
class ThemeState with _$ThemeState {
  const factory ThemeState.initial({
    @Default(ThemeMode.light) ThemeMode themeMode,
  }) = _Initial;
}
