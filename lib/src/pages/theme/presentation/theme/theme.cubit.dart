import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:memolary_mobile_revamp/src/core/core.dart';
import 'package:memolary_mobile_revamp/src/pages/theme/domain/use_case/get_theme_use_case.dart';

part 'theme.state.dart';
part 'theme.cubit.freezed.dart';

@singleton
class ThemeCubit extends Cubit<ThemeState> {
  final GetThemeUsecase getThemeUsecase;
  ThemeCubit({required this.getThemeUsecase})
      : super(const ThemeState.initial());

  ThemeMode getTheme() {
    final response = getThemeUsecase.call(NoParams());
    ThemeMode themeMode = response.fold((l) => ThemeMode.light, (r) => r);
    return themeMode;
  }

  void setTheme(ThemeMode themeMode) {
    emit(
      state.copyWith(themeMode: themeMode),
    );
  }

  void changeTheme() {
    bool isLight = state.themeMode == ThemeMode.light;
    setTheme(isLight ? ThemeMode.dark : ThemeMode.light);
  }
}
