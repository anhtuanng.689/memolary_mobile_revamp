import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/core.dart';
import '../../domain/repository/theme_repository.dart';
import '../data_source/local/theme_local_data_source.dart';

@Injectable(as: ThemeRepository)
class ThemeRepositoryImpl implements ThemeRepository {
  final ThemeLocalDataSource themeLocalDataSource;

  ThemeRepositoryImpl({
    required this.themeLocalDataSource,
  });

  @override
  Either<Failure, ThemeMode> getTheme() {
    final themeMode = themeLocalDataSource.getTheme();
    return Right(themeMode);
  }
}
