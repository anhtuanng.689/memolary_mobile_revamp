import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';

import '../../../../../constants/constants.dart';

abstract class ThemeLocalDataSource {
  ThemeMode getTheme();
}

@Injectable(as: ThemeLocalDataSource)
class ThemeLocalDataSourceImpl implements ThemeLocalDataSource {
  @override
  ThemeMode getTheme() {
    // hiveBox.delete(StorageKeys.accessToken);
    return ThemeMode.light;
  }
}
