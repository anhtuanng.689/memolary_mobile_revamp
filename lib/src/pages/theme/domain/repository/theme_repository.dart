import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

import '../../../../core/core.dart';

abstract class ThemeRepository {
  Either<Failure, ThemeMode> getTheme();
}
