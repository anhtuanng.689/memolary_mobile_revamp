import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/core.dart';
import '../repository/theme_repository.dart';

@injectable
class GetThemeUsecase implements UseCase<ThemeMode, NoParams> {
  final ThemeRepository themeRepository;

  GetThemeUsecase({required this.themeRepository});

  @override
  Either<Failure, ThemeMode> call(NoParams params) {
    return themeRepository.getTheme();
  }
}
