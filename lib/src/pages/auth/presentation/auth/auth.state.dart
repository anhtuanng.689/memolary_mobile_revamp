part of 'auth.bloc.dart';

@freezed
class AuthState with _$AuthState {
  const factory AuthState.initial({
    @Default(AuthStatus.initial) AuthStatus status,
  }) = _Initial;
}

enum AuthStatus {
  initial,
  loading,
  authenticated,
  unauthenticated,
}
