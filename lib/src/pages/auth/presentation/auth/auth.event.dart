part of 'auth.bloc.dart';

@freezed
class AuthEvent with _$AuthEvent {
  // const factory AuthEvent.signInSuccess() = _SignInSuccess;
  const factory AuthEvent.checkAuthenticated() = CheckAuthenticated;
  const factory AuthEvent.signOut() = SignOut;
}
