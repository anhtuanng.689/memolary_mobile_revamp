import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/core.dart';
import '../../domain/use_case/check_signed_in_use_case.dart';
import '../../domain/use_case/sign_out_use_case.dart';

part 'auth.event.dart';
part 'auth.state.dart';
part 'auth.bloc.freezed.dart';

@singleton
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final CheckSignedInUsecase checkSignedInUsecase;
  final SignOutUsecase signOutUsecase;
  AuthBloc({
    required this.checkSignedInUsecase,
    required this.signOutUsecase,
  }) : super(const AuthState.initial()) {
    on<CheckAuthenticated>((event, emit) async {
      emit(state.copyWith(status: AuthStatus.loading));

      final response = checkSignedInUsecase.call(NoParams());
      await Future.delayed(const Duration(seconds: 3));
      response.fold((l) {
        emit(state.copyWith(status: AuthStatus.unauthenticated));
      }, (r) {
        emit(state.copyWith(status: AuthStatus.authenticated));
      });
    });
    on<SignOut>(
      (event, emit) {
        final response = signOutUsecase.call(NoParams());
        response.fold((l) {}, (r) {
          emit(state.copyWith(status: AuthStatus.unauthenticated));
        });
      },
    );
  }
}
