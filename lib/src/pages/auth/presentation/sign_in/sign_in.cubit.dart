import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:go_router/go_router.dart';
import 'package:injectable/injectable.dart';
import 'package:memolary_mobile_revamp/src/constants/constants.dart';

import '../../../../core/core.dart';
import '../../domain/use_case/sign_in_use_case.dart';
import '../auth/auth.bloc.dart';

part 'sign_in.state.dart';
part 'sign_in.cubit.freezed.dart';

@injectable
class SignInCubit extends Cubit<SignInState> {
  final SignInUsecase signInUsecase;
  SignInCubit({
    required this.signInUsecase,
  }) : super(const SignInState.initial());

  void emailChanged(String value) {
    final email = value.trim();
    emit(state.copyWith(
      email: value,
      emailError: Validators().checkEmail(email),
    ));
  }

  void passwordChanged(String value) {
    final password = value.trim();
    emit(state.copyWith(
      password: value,
      passwordError: Validators().checkPass(password),
    ));
  }

  void signIn(BuildContext context) async {
    emit(state.copyWith(status: SignInStatus.loading));
    final response = await signInUsecase
        .call(SignInParam(email: state.email, password: state.password));
    response.fold((l) {
      emit(state.copyWith(status: SignInStatus.error));
    }, (r) {
      context.read<AuthBloc>().add(const AuthEvent.checkAuthenticated());
      emit(state.copyWith(status: SignInStatus.success));
    });
  }
}
