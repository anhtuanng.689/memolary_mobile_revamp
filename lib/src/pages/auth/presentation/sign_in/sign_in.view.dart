import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../../../../config/config.dart';
import '../../../../constants/constants.dart';
import '../../../../widgets/widgets.dart';
// import '../auth/auth.cubit.dart';
import 'sign_in.cubit.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormFieldState> _key = GlobalKey<FormFieldState>();

    return SafeArea(
      child: BlocConsumer<SignInCubit, SignInState>(
        listener: (context, state) {
          if (state.status == SignInStatus.error) {
            DSToast.show(context,
                msg: 'Something went wrong, please try again');
          } else if (state.status == SignInStatus.success) {
            DSToast.show(context, msg: 'Signed in success');
            context.go(RouteConstants.homePage);
          }
        },
        builder: (context, state) {
          return Stack(
            children: [
              Scaffold(
                body: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Sign In',
                          style: TextStyles.h2,
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        Form(
                          key: _key,
                          child: Column(
                            children: [
                              DSTextField(
                                labelText: 'Email',
                                hint: 'Please enter your email',
                                clear: true,
                                onChanged: (value) => context
                                    .read<SignInCubit>()
                                    .emailChanged(value),
                                error: state.emailError,
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              DSTextField(
                                labelText: 'Password',
                                hint: 'Please enter your password',
                                clear: true,
                                isPassword: true,
                                obscureText: true,
                                onChanged: (value) => context
                                    .read<SignInCubit>()
                                    .passwordChanged(value),
                                error: state.passwordError,
                              ),
                              const SizedBox(
                                height: 24,
                              ),
                              DSButton3D(
                                onPressed: () =>
                                    context.read<SignInCubit>().signIn(context),
                                child: Text(
                                  'Sign In',
                                  style: TextStyles.button1,
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              state.status == SignInStatus.loading
                  ? const DSLoading()
                  : const SizedBox.shrink(),
            ],
          );
        },
      ),
    );
  }
}
