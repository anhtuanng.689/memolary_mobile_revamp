part of 'sign_in.cubit.dart';

@freezed
class SignInState with _$SignInState {
  const factory SignInState.initial({
    @Default('') String email,
    @Default('') String password,
    @Default('') String emailError,
    @Default('') String passwordError,
    @Default(SignInStatus.initial) SignInStatus status,
  }) = _Initial;
}

enum SignInStatus {
  initial,
  loading,
  success,
  error,
}
