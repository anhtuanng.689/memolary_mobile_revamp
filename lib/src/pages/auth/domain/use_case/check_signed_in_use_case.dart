import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/core.dart';
import '../repository/auth_repository.dart';

@injectable
class CheckSignedInUsecase implements UseCase<bool, NoParams> {
  final AuthRepository authRepository;

  CheckSignedInUsecase({required this.authRepository});

  @override
  Either<Failure, bool> call(NoParams params) {
    return authRepository.isSignedIn();
  }
}
