import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/core.dart';
import '../repository/auth_repository.dart';

@injectable
class SignOutUsecase implements UseCase<bool, NoParams> {
  final AuthRepository authRepository;

  SignOutUsecase({required this.authRepository});

  @override
  Either<Failure, bool> call(NoParams params) {
    return authRepository.signOut();
  }
}
