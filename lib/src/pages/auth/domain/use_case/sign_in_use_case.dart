import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/core.dart';
import '../repository/auth_repository.dart';

@injectable
class SignInUsecase implements UseCaseFuture<bool, SignInParam> {
  final AuthRepository authRepository;

  SignInUsecase({required this.authRepository});

  @override
  Future<Either<Failure, bool>> call(SignInParam params) async {
    return await authRepository.signIn(params);
  }
}

class SignInParam {
  final String email;
  final String password;
  SignInParam({
    required this.email,
    required this.password,
  });
}
