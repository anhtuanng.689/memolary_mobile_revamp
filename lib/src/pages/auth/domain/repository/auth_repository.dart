import 'package:dartz/dartz.dart';

import '../../../../core/core.dart';
import '../use_case/sign_in_use_case.dart';

abstract class AuthRepository {
  Future<Either<Failure, bool>> signIn(SignInParam param);

  Either<Failure, bool> isSignedIn();

  Either<Failure, bool> signOut();
}
