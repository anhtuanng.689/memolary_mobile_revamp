import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/core.dart';
import '../../domain/repository/auth_repository.dart';
import '../../domain/use_case/sign_in_use_case.dart';
import '../data_source/local/auth_local_data_source.dart';
import '../data_source/remote/auth_remote_data_source.dart';
import '../data_source/remote/sign_in_request.dart';
import '../../data/model/auth_model.dart';

@Injectable(as: AuthRepository)
class AuthRepositoryImpl implements AuthRepository {
  final AuthRemoteDataSource authRemoteDataSource;
  final AuthLocalDataSource authLocalDataSource;

  AuthRepositoryImpl({
    required this.authRemoteDataSource,
    required this.authLocalDataSource,
  });

  @override
  Future<Either<Failure, bool>> signIn(SignInParam param) async {
    try {
      var response = await authRemoteDataSource
          .signIn(SignInRequest(email: param.email, password: param.password));
      var authEntity = response.toEntity;

      if (authEntity.accessToken.isNotEmpty) {
        authLocalDataSource.saveAccessToken(authEntity.accessToken);
        return const Right(true);
      }

      return const Right(false);
    } catch (error) {
      return Left(ServerFailure(error.toString()));
    }
  }

  @override
  Either<Failure, bool> isSignedIn() {
    String? token = authLocalDataSource.getAccessToken();

    if (token == null) {
      return Left(NullValue());
    }

    return const Right(true);
  }

  @override
  Either<Failure, bool> signOut() {
    authLocalDataSource.clearAccessToken();
    return const Right(true);
  }
}
