import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:memolary_mobile_revamp/src/pages/auth/domain/entity/auth_entity.dart';

part 'auth_model.freezed.dart';
part 'auth_model.g.dart';

@freezed
class AuthModel with _$AuthModel {
  factory AuthModel({
    required String accessToken,
  }) = _AuthModel;

  factory AuthModel.fromJson(Map<String, dynamic> json) =>
      _$AuthModelFromJson(json);
}

extension AuthModelX on AuthModel {
  AuthEntity get toEntity => AuthEntity(accessToken: accessToken);
}