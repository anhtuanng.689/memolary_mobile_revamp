import 'package:injectable/injectable.dart';

import '../../../../../network/network.dart';
import '../../model/auth_model.dart';
import 'sign_in_request.dart';


abstract class AuthRemoteDataSource {
  Future<AuthModel> signIn(SignInRequest request);
}

@Injectable(as: AuthRemoteDataSource)
class AuthRemoteDataSourceImpl extends DioClient
    implements AuthRemoteDataSource {
  AuthRemoteDataSourceImpl({required super.dioClient});

  @override
  Future<AuthModel> signIn(SignInRequest request) async {
    final response = await dioClient.post(
      '/auth/login',
      data: request.toJson(),
    );
    return AuthModel.fromJson(response.data);
  }
}
