import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';

import '../../../../../constants/constants.dart';

abstract class AuthLocalDataSource {
  String? getAccessToken();
  void saveAccessToken(String accessToken);
  void clearAccessToken();
}

@Injectable(as: AuthLocalDataSource)
class AuthLocalDataSourceImpl implements AuthLocalDataSource {
  final Box hiveBox = Hive.box(StorageKeys.boxAuth);

  @override
  void clearAccessToken() {
    hiveBox.delete(StorageKeys.accessToken);
  }

  @override
  String? getAccessToken() {
    return hiveBox.get(StorageKeys.accessToken);
  }

  @override
  void saveAccessToken(String accessToken) {
    hiveBox.put(StorageKeys.accessToken, accessToken);
  }
}
