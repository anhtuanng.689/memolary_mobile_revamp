import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:memolary_mobile_revamp/src/widgets/widgets.dart';

import '../../constants/constants.dart';
import '../theme/presentation/theme/theme.cubit.dart';
import '../auth/presentation/auth/auth.bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBloc, AuthState>(
      listenWhen: (previous, current) {
        return previous.status == AuthStatus.authenticated &&
            current.status == AuthStatus.unauthenticated;
      },
      listener: (context, state) {
        DSToast.show(context, msg: 'Signed out success');

        context.go(RouteConstants.signInPage);
      },
      child: Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                context.read<ThemeCubit>().changeTheme();
              },
              icon: const Icon(Icons.dark_mode),
            ),
          ],
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Home: ${context.watch<AuthBloc>().state.status.toString()}'),
            DSButton3D(
              onPressed: () {
                context.read<AuthBloc>().add(const AuthEvent.signOut());
              },
              child: const Text('Sign out'),
            ),
            DSButton3D(
              onPressed: () {
                context.push(
                    '${RouteConstants.homePage}/${RouteConstants.coursesPage}');
              },
              child: const Text('Courses'),
            ),
          ],
        ),
      ),
    );
  }
}
