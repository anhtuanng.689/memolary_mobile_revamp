import 'package:freezed_annotation/freezed_annotation.dart';

import '../../domain/entity/course_entity.dart';

part 'courses_model.freezed.dart';
part 'courses_model.g.dart';

@freezed
class CoursesModel with _$CoursesModel {
  factory CoursesModel({required List<CourseModel> data}) = _CoursesModel;

  factory CoursesModel.fromJson(Map<String, dynamic> json) =>
      _$CoursesModelFromJson(json);
}

@freezed
class CourseModel with _$CourseModel {
  factory CourseModel({
    String? id,
    String? name,
    String? image,
    int? number,
    String? description,
    int? viewCount,
  }) = _CourseModel;

  factory CourseModel.fromJson(Map<String, dynamic> json) =>
      _$CourseModelFromJson(json);
}

extension CoursesModelX on CoursesModel {
  List<CourseEntity> get toEntities => data
      .map((e) => CourseEntity(
          id: e.id ?? '',
          name: e.name ?? '',
          image: e.image ?? '',
          number: e.number ?? 0,
          description: e.description ?? '',
          viewCount: e.viewCount ?? 0))
      .toList();
}
