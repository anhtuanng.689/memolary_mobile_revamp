import 'package:injectable/injectable.dart';

import '../../../../../network/network.dart';
import '../../model/courses_model.dart';

abstract class CoursesRemoteDataSource {
  Future<CoursesModel> getCourses();
}

@Injectable(as: CoursesRemoteDataSource)
class CoursesRemoteDataSourceImpl extends DioClient
    implements CoursesRemoteDataSource {
  CoursesRemoteDataSourceImpl({required super.dioClient});

  @override
  Future<CoursesModel> getCourses() async {
    final response = await dioClient.get(
      '/courses/all',
    );
    return CoursesModel.fromJson(response.data);
  }
}
