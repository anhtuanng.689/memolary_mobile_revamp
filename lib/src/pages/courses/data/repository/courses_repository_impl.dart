import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import '../../data/model/courses_model.dart';
import '../../../../core/core.dart';
import '../../domain/entity/course_entity.dart';
import '../../domain/repository/courses_repository.dart';
import '../data_source/remote/courses_remote_data_source.dart';

@Injectable(as: CoursesRepository)
class CoursesRepositoryImpl implements CoursesRepository {
  final CoursesRemoteDataSource coursesRemoteDataSource;

  CoursesRepositoryImpl({
    required this.coursesRemoteDataSource,
  });

  @override
  Future<Either<Failure, List<CourseEntity>>> getCourses() async {
    try {
      var response = await coursesRemoteDataSource.getCourses();

      return Right(response.toEntities);
    } catch (error) {
      return Left(ServerFailure(error.toString()));
    }
  }
}
