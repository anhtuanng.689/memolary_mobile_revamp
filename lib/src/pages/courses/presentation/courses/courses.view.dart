import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../../../../config/config.dart';
import '../../../../widgets/ds_card.dart';
import '../../../../widgets/widgets.dart';
import 'courses.cubit.dart';

class CoursesPage extends StatefulWidget {
  const CoursesPage({super.key});

  @override
  State<CoursesPage> createState() => _CoursesPageState();
}

class _CoursesPageState extends State<CoursesPage> {
  @override
  void initState() {
    WidgetsBinding.instance
        .addPostFrameCallback((_) => context.read<CoursesCubit>().getCourses());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: BlocBuilder<CoursesCubit, CoursesState>(
        builder: (context, state) {
          return state.status == CoursesStatus.loading
              ? const SizedBox.shrink()
              : Stack(
                  children: [
                    Scaffold(
                      appBar: AppBar(
                        leading: IconButton(
                          onPressed: () {
                            context.pop();
                          },
                          icon: const Icon(Icons.arrow_back_ios),
                        ),
                      ),
                      body: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.all(16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Courses',
                                style: TextStyles.h2,
                              ),
                              const SizedBox(
                                height: 24,
                              ),
                              ...state.courses
                                  .map((e) => DSCard(
                                        title: e.name,
                                        image: e.image,
                                        number: e.number.toString(),
                                      ))
                                  .toList(),
                            ],
                          ),
                        ),
                      ),
                    ),
                    state.status == CoursesStatus.loading
                        ? const DSLoading()
                        : const SizedBox.shrink(),
                  ],
                );
        },
      ),
    );
  }
}
