import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:memolary_mobile_revamp/src/core/core.dart';

import '../../domain/entity/course_entity.dart';
import '../../domain/use_case/get_courses_use_case.dart';

part 'courses.state.dart';
part 'courses.cubit.freezed.dart';

@injectable
class CoursesCubit extends Cubit<CoursesState> {
  final GetCoursesUsecase getCoursesUsecase;
  CoursesCubit({
    required this.getCoursesUsecase,
  }) : super(const CoursesState.initial());

  void getCourses() async {
    emit(state.copyWith(status: CoursesStatus.loading));
    final response = await getCoursesUsecase.call(NoParams());
    response.fold((l) {
      emit(state.copyWith(status: CoursesStatus.error));
    }, (r) {
      emit(state.copyWith(status: CoursesStatus.success, courses: r));
    });
  }
}
