part of 'courses.cubit.dart';

@freezed
class CoursesState with _$CoursesState {
  const factory CoursesState.initial({
    @Default([]) List<CourseEntity> courses,
    @Default(CoursesStatus.initial) CoursesStatus status,
  }) = _Initial;
}

enum CoursesStatus {
  initial,
  loading,
  success,
  error,
}
