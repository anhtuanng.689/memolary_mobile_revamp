class CourseEntity {
  final String id;
  final String name;
  final String image;
  final int number;
  final String description;
  final int viewCount;
  CourseEntity({
    required this.id,
    required this.name,
    required this.image,
    required this.number,
    required this.description,
    required this.viewCount,
  });
}
