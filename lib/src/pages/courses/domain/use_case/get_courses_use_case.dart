import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/core.dart';
import '../entity/course_entity.dart';
import '../repository/courses_repository.dart';

@injectable
class GetCoursesUsecase implements UseCaseFuture<List<CourseEntity>, NoParams> {
  final CoursesRepository coursesRepository;

  GetCoursesUsecase({required this.coursesRepository});

  @override
  Future<Either<Failure, List<CourseEntity>>> call(NoParams params) async {
    return await coursesRepository.getCourses();
  }
}

// class GetCoursesParam {
//   final String email;
//   final String password;
//   GetCoursesParam({
//     required this.email,
//     required this.password,
//   });
// }
