import 'package:dartz/dartz.dart';

import '../../../../core/core.dart';
import '../entity/course_entity.dart';

abstract class CoursesRepository {
  Future<Either<Failure, List<CourseEntity>>> getCourses();
}
