import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../config/config.dart';

class DSLoading extends StatelessWidget {
  const DSLoading({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: SpinKitPulsingGrid(
        color: AppColors.greenColor,
        size: 50.0,
      ),
    );
  }
}
