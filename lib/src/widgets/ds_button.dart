library flutter_buttons;

import 'package:flutter/material.dart';

import '../config/config.dart';

class StyleOf3dButton {
  final Color topColor;
  final Color backColor;
  final BorderRadius borderRadius;
  final double z;
  final double tapped;

  const StyleOf3dButton({
    this.topColor = const Color(0xFF45484c),
    this.backColor = const Color(0xFF191a1c),
    this.borderRadius = const BorderRadius.all(
      Radius.circular(7.0),
    ),
    this.z = 8.0,
    this.tapped = 3.0,
  });
  static const NORMAL = StyleOf3dButton(
    topColor: AppColors.buttonColor,
    backColor: AppColors.buttonColor,
    borderRadius: BorderRadius.all(
      Radius.circular(20),
    ),
  );
  static const SMALL = StyleOf3dButton(
    topColor: AppColors.buttonColor,
    backColor: AppColors.buttonColor,
    borderRadius: BorderRadius.all(
      Radius.circular(25),
    ),
  );
}

class DSButton3D extends StatefulWidget {
  final VoidCallback onPressed;
  final Widget child;
  final StyleOf3dButton style;
  final double? width;
  final double? height;

  DSButton3D({
    required this.onPressed,
    required this.child,
    this.style = StyleOf3dButton.NORMAL,
    this.width,
    this.height,
  });

  @override
  State<StatefulWidget> createState() => DSButton3DState();
}

class DSButton3DState extends State<DSButton3D> {
  bool isTapped = false;

  double getHeight() {
    return 60;
  }

  double getWidth() {
    if (widget.style == StyleOf3dButton.NORMAL) {
      return double.maxFinite;
    }
    if (widget.style == StyleOf3dButton.SMALL) {
      return 120;
    }
    return 120;
  }

  Widget _buildBackLayout() {
    return Padding(
      padding: EdgeInsets.only(top: widget.style.z),
      child: DecoratedBox(
        position: DecorationPosition.background,
        decoration: BoxDecoration(
          borderRadius: widget.style.borderRadius,
          boxShadow: [
            BoxShadow(
              color: widget.style.backColor,
              offset: const Offset(2, 0),
            )
          ],
        ),
        child: ConstrainedBox(
          constraints: BoxConstraints.expand(
            width: widget.width ?? getWidth(),
            height: (widget.height ?? getHeight()) - widget.style.z,
          ),
        ),
      ),
    );
  }

  Widget _buildTopLayout() {
    return Padding(
      padding: EdgeInsets.only(
        top: isTapped ? widget.style.z - widget.style.tapped : 3,
      ),
      child: DecoratedBox(
        position: DecorationPosition.background,
        decoration: BoxDecoration(
          borderRadius: widget.style.borderRadius,
          boxShadow: [
            BoxShadow(
              color: widget.style.topColor,
            ),
          ],
        ),
        child: ConstrainedBox(
          constraints: BoxConstraints.expand(
            width: widget.width ?? getWidth(),
            height: (widget.height ?? getHeight()) - widget.style.z,
          ),
          child: Container(
            padding: EdgeInsets.zero,
            alignment: Alignment.center,
            child: widget.child,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> children = [_buildBackLayout(), _buildTopLayout()];
    return GestureDetector(
      child: Stack(
        alignment: AlignmentDirectional.topCenter,
        children: children,
      ),
      onTapDown: (TapDownDetails event) {
        setState(() {
          isTapped = true;
        });
      },
      onTapCancel: () {
        setState(() {
          isTapped = false;
        });
      },
      onTapUp: (TapUpDetails event) {
        setState(() {
          isTapped = false;
        });
        FocusManager.instance.primaryFocus?.unfocus();
        widget.onPressed();
      },
    );
  }
}
