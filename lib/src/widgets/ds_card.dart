import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../config/config.dart';

class DSCard extends StatelessWidget {
  final String? title;
  final String? image;
  final String? number;
  const DSCard({
    Key? key,
    this.title,
    this.image,
    this.number,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        image != null && image!.isNotEmpty
            ? Expanded(
                child: Image.network(
                  image!,
                  fit: BoxFit.fill,
                ),
              )
            : const SizedBox.shrink(),
        Column(
          children: [
            Text(
              title ?? '',
              style: TextStyles.title1,
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              number ?? '',
              style: TextStyles.title2,
            ),
          ],
        ),
        const Icon(Icons.arrow_right),
      ],
    );
  }
}
