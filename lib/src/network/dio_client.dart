import 'package:dio/dio.dart';
import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';
import '../constants/constants.dart';
import 'interceptors/custom_interceptor.dart';

@module
abstract class DioModule {
  @Named("baseUrl")
  String get baseUrl => 'http://192.168.1.26:3000';

  @Named("accessToken")
  String get accessToken =>
      Hive.box(StorageKeys.boxAuth).get(StorageKeys.accessToken);

  @lazySingleton
  Dio dio(
      @Named('baseUrl') String url, @Named('accessToken') String accessToken) {
    final Dio dio = Dio(BaseOptions(
      baseUrl: url,
      connectTimeout: const Duration(seconds: 10),
      receiveTimeout: const Duration(seconds: 10),
    ))
      ..interceptors.add(CustomInterceptor(accessToken: accessToken));
    return dio;
  }
}

class DioClient extends DioModule {
  Dio dioClient;
  DioClient({
    required this.dioClient,
  });
}
