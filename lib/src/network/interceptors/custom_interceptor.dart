// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dio/dio.dart';

import '../../core/core.dart';

class CustomInterceptor extends Interceptor {
  final String? accessToken;
  CustomInterceptor({
    this.accessToken,
  });

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    if (accessToken != null && accessToken!.isNotEmpty) {
      options.headers["Authorization"] = "Bearer $accessToken";
    }
    logi.d('REQUEST[${options.method}] => PATH: ${options.path}');
    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    logi.d(
        'RESPONSE[${response.statusCode}] => PATH: ${response.realUri.path}\n${response.data}');
    switch (response.statusCode) {
      case 200:
      case 201:
      case 202:
        return super.onResponse(response, handler);
      default:
        throw ServerFailure('Server error: ${response.statusMessage}');
    }
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    logi.e(
        'ERROR[${err.response?.statusCode}] => PATH: ${err.response?.realUri.path}');
    throw ServerFailure(
        'Server error: ${err.response?.statusCode} ${err.response?.statusMessage}');
  }
}
