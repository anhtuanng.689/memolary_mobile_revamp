// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i7;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../network/dio_client.dart' as _i23;
import '../pages/auth/data/data_source/local/auth_local_data_source.dart'
    as _i3;
import '../pages/auth/data/data_source/remote/auth_remote_data_source.dart'
    as _i10;
import '../pages/auth/data/repository/auth_repository_impl.dart' as _i12;
import '../pages/auth/domain/repository/auth_repository.dart' as _i11;
import '../pages/auth/domain/use_case/check_signed_in_use_case.dart' as _i13;
import '../pages/auth/domain/use_case/sign_in_use_case.dart' as _i18;
import '../pages/auth/domain/use_case/sign_out_use_case.dart' as _i19;
import '../pages/auth/presentation/auth/auth.bloc.dart' as _i20;
import '../pages/auth/presentation/sign_in/sign_in.cubit.dart' as _i22;
import '../pages/courses/data/data_source/remote/courses_remote_data_source.dart'
    as _i14;
import '../pages/courses/data/repository/courses_repository_impl.dart' as _i16;
import '../pages/courses/domain/repository/courses_repository.dart' as _i15;
import '../pages/courses/domain/use_case/get_courses_use_case.dart' as _i17;
import '../pages/courses/presentation/courses/courses.cubit.dart' as _i21;
import '../pages/theme/data/data_source/local/theme_local_data_source.dart'
    as _i4;
import '../pages/theme/data/repository/theme_repository_impl.dart' as _i6;
import '../pages/theme/domain/repository/theme_repository.dart' as _i5;
import '../pages/theme/domain/use_case/get_theme_use_case.dart' as _i8;
import '../pages/theme/presentation/theme/theme.cubit.dart' as _i9;

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// initializes the registration of main-scope dependencies inside of GetIt
_i1.GetIt $initGetIt(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  final dioModule = _$DioModule();
  gh.factory<_i3.AuthLocalDataSource>(() => _i3.AuthLocalDataSourceImpl());
  gh.factory<String>(
    () => dioModule.baseUrl,
    instanceName: 'baseUrl',
  );
  gh.factory<String>(
    () => dioModule.accessToken,
    instanceName: 'accessToken',
  );
  gh.factory<_i4.ThemeLocalDataSource>(() => _i4.ThemeLocalDataSourceImpl());
  gh.factory<_i5.ThemeRepository>(() => _i6.ThemeRepositoryImpl(
      themeLocalDataSource: gh<_i4.ThemeLocalDataSource>()));
  gh.lazySingleton<_i7.Dio>(() => dioModule.dio(
        gh<String>(instanceName: 'baseUrl'),
        gh<String>(instanceName: 'accessToken'),
      ));
  gh.factory<_i8.GetThemeUsecase>(
      () => _i8.GetThemeUsecase(themeRepository: gh<_i5.ThemeRepository>()));
  gh.singleton<_i9.ThemeCubit>(
      _i9.ThemeCubit(getThemeUsecase: gh<_i8.GetThemeUsecase>()));
  gh.factory<_i10.AuthRemoteDataSource>(
      () => _i10.AuthRemoteDataSourceImpl(dioClient: gh<_i7.Dio>()));
  gh.factory<_i11.AuthRepository>(() => _i12.AuthRepositoryImpl(
        authRemoteDataSource: gh<_i10.AuthRemoteDataSource>(),
        authLocalDataSource: gh<_i3.AuthLocalDataSource>(),
      ));
  gh.factory<_i13.CheckSignedInUsecase>(() =>
      _i13.CheckSignedInUsecase(authRepository: gh<_i11.AuthRepository>()));
  gh.factory<_i14.CoursesRemoteDataSource>(
      () => _i14.CoursesRemoteDataSourceImpl(dioClient: gh<_i7.Dio>()));
  gh.factory<_i15.CoursesRepository>(() => _i16.CoursesRepositoryImpl(
      coursesRemoteDataSource: gh<_i14.CoursesRemoteDataSource>()));
  gh.factory<_i17.GetCoursesUsecase>(() =>
      _i17.GetCoursesUsecase(coursesRepository: gh<_i15.CoursesRepository>()));
  gh.factory<_i18.SignInUsecase>(
      () => _i18.SignInUsecase(authRepository: gh<_i11.AuthRepository>()));
  gh.factory<_i19.SignOutUsecase>(
      () => _i19.SignOutUsecase(authRepository: gh<_i11.AuthRepository>()));
  gh.singleton<_i20.AuthBloc>(_i20.AuthBloc(
    checkSignedInUsecase: gh<_i13.CheckSignedInUsecase>(),
    signOutUsecase: gh<_i19.SignOutUsecase>(),
  ));
  gh.factory<_i21.CoursesCubit>(
      () => _i21.CoursesCubit(getCoursesUsecase: gh<_i17.GetCoursesUsecase>()));
  gh.factory<_i22.SignInCubit>(
      () => _i22.SignInCubit(signInUsecase: gh<_i18.SignInUsecase>()));
  return getIt;
}

class _$DioModule extends _i23.DioModule {}
