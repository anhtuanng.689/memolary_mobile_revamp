class StorageKeys {
  // Box Variables
  static const String boxAuth = 'boxAuth';

  // Key in Box - Auth
  static const String accessToken = 'accessToken';
}
