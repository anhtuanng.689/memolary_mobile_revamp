import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../widgets/widgets.dart';

class RouteConstants {
  static const splashName = 'splash';
  static const splashPage = '/splash';

  static const homeName = 'home';
  static const homePage = '/home';

  static const signInName = 'signIn';
  static const signInPage = '/signIn';

  static const coursesName = 'courses';
  static const coursesPage = 'courses';

  static Widget errorWidget(BuildContext context, GoRouterState state) =>
      const NotFoundPage();
}
