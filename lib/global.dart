import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memolary_mobile_revamp/src/di/di.dart';

import 'observer.dart';
import 'src/config/config.dart';

class Global {
  static Future init() async {
    Bloc.observer = SimpleBlocObserver();
    await HiveStorage.initialBox();
    await configureDependencies();
  }
}
